import jwt from 'jsonwebtoken'
import {secret} from '../helpers/config.js'

const roleMiddleware = (role) => (req, res, next)=>{
    if(req.method === "OPTIONS"){
        next()
    }
    try{
        const token = req.headers.authorization.split(" ")[1]
        if(!token){
            return res.status(403).json({message: "Пользователь не авторизован"}) 
        }
        const userRole = jwt.verify(token, secret).role
        let hasRole = false
        if(userRole.toString().includes(role.toString())){
            hasRole = true
        }
        if(!hasRole){
            return res.status(403).json({message: "У вас нет доступа"})
        }
        next()
    }catch(e){
        return res.status(403).json({message: "У вас нет доступа"})
    }
}

export default roleMiddleware