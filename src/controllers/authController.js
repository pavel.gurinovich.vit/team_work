import User from "../models/User.js"
import bCrypt from "bcrypt"
import generateAccessToken from '../helpers/generateAccessToken.js'
import jwt from 'jsonwebtoken'

import {google}  from 'googleapis';
import urlParse from 'url-parse';
import queryString from 'query-string';
import axios from 'axios';

import {secret} from '../helpers/config.js'

const clientId = '915349853967-s9t0ln4vg2nssadmor3cip4uqon713l7.apps.googleusercontent.com';
const clientSecret = 'qsNkOKN_vujkJoHHjtbukEWr';
const redirect = 'http://localhost:3000/auth/google/redirect';

const oauth2Client = new google.auth.OAuth2(
    clientId,
    clientSecret,
    redirect,
);


class authController {
    async registration(req, res){
        try{
            const {email, password, name, surname, role} = req.body
            const candidate = await User.findOne({email})
            if(candidate){
                return res.status(400).json({message:"Пользователь с такой почтой уже существует"})
            }
            const hashPassword = bCrypt.hashSync(password, 7)
            const user = new User({email, password: hashPassword, name, surname, role})
            await user.save()

            return res.json({message: "Пользователь зарегестрирован"})
        }catch (e){
            res.status(400).json({message: "Registration error -> " + e})
        }
    }

    async login(req,res){
        try{
            const {email, password} = req.body
            const user = await User.findOne({email})
            if(!user){
                return res.status(400).json({message: `Пользователь ${email} не найден`})
            }
            const validPassword = bCrypt.compareSync(password, user.password)
            if(!validPassword){
                return res.status(400).json({message: `Введен неверный пароль`})
            }
            const token = generateAccessToken(user._id, user.role)
            
            return res.json({token: token})
        }catch(e){
            res.status(400).json({message: "Registration error -> " + e})
        }
    }

    
    async createGoogleUrl (req, res){
        try{
            const url = oauth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: ['https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email']
            });
            res.send(url);
        }catch(err){
            res.status(403).send(`Can't create url`)
        }
    }
    
    async getToken(req, res){
            try{
                const queryURL = new urlParse(req.url);
                const code = queryString.parse(queryURL.query).code;
                console.log(code);
                const {tokens} = await oauth2Client.getToken(code);
                // console.log(token)
                //res.send(tokens)

            const {data} = await axios({
                    url: 'https://www.googleapis.com/oauth2/v2/userinfo',
                    method: 'get',
                    headers: {
                        authorization: `Bearer ${tokens.access_token}`
                    }
                })
                //res.send(data)

                let user = await User.findOne({email: data.email});

                if(!user){
                    const newUser = new User({email: data.email,name: data.given_name, surname: data.family_name});
                    await newUser.save();

                    const accessToken = jwt.sign({name: newUser.name, role: newUser.role, _id: newUser._id}, secret, {expiresIn: '56565656m'});

                    res.send(`Create new user: ${newUser.name} ${newUser.surname}, accessToken: ${accessToken}`)
                }
                else{
                    res.status(403).send(`Can't get token`)
                }
            }   catch(err) {
                console.log(err)
                res.status(500).send(`You'r google account was already used for authorization`)
            }
    }
}

export default new authController
