import Category from "../models/Category.js"
import Product from "../models/Product.js"

class ProductController{
    async productCreate(req, res){
        try{
            const {name} = req.body
            const product = await Product.findOne({name})
            if(product){
                return res.status(400).json({message:"Такой продукт уже существует"})
            }
            const obj = await Product.create(req.body)
            addProductToCategory(obj._id, category)
            res.json({message: obj})
        }catch (e){
            res.status(400).json({message: "Adding product error -> " + e})
        }
    }

    async deleteProduct (req, res) {
        try{
            const product = await Product.findOne({_id: req.params.id})
            if(product){
                deleteFromCategory(product.category, req.params.id)
                Product.deleteOne({_id: req.params.id}, function(err, doc) {
                    if (err) return res.send("error");
                    res.send(`Продукт удален`);
                });
            }
            else res.send(`Такого продукта не существует`);
             
        }catch (e){
            res.status(400).json({message: "Deleting product error -> " + e})
        }
    }

    async getById (req, res){
        try{
            const product = await Product.findOne({_id: req.params.id})
            res.json({product})
        }catch (e){
            res.status(400).json({message: "Get Product ID error -> " + e})
        } 
    } 

    async getAll (req, res){
        try{
            const products = await Product.find();
            res.json(products);   
        }catch (e){
            res.status(400).json({message: "Get Products list error -> " + e})
        }
    }

    async updateProduct (req, res){
        try{
            const product = await Product.findOne({_id: req.params.id});
            if(!product){
                return res.status(400).json({message:"Нет такого продукта"})
            }
            const {name, price, category} = req.body;
            console.log(category);
            res.send({ message: category })
            if(category || category===null){
                deleteFromCategory(product.category, product._id)
            }
            await Product.updateOne({_id: req.params.id}, {name, price, category}) 
            addProductToCategory(product._id, category)
            res.json({message:"Продукт изменен"})
        }catch (e){
            res.status(400).json({message: "Update product error -> " + e})
        } 
    }
    
}

const addProductToCategory = async(productId, categoryId)=>{
    const category = await Category.findOne({_id: categoryId})
    category.products.push(productId)
    category.save()
}
    
const deleteFromCategory = async (categoryId, productId)=>{
    let category = await Category.findOne({_id: categoryId})
    for(let i=0;i<category.products.length;i++){
        if(category.products[i].toString() === productId.toString()){
            category.products.splice(i, 1)
            await category.save()
            break
        }
    }
} 
export default new ProductController;

