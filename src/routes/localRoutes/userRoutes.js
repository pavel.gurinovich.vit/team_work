import express from 'express'

import authMiddleware from '../../middleware/authMiddleware.js'
import roleMiddleware from '../../middleware/roleMiddleware.js'

import userController from '../../controllers/userController.js' 
const router = express.Router()

router.post("/create", roleMiddleware('ADMIN'),userController.userCreate);    
router.delete("/:id", roleMiddleware('ADMIN'), userController.deleteUser);
router.get("/getbyid/:id", authMiddleware, userController.getById);
router.get("/getall", authMiddleware, userController.getAll);
router.put("/:id", authMiddleware, userController.updateUser);


export default router;
