import express from 'express'
import categoryController from '../../controllers/categoryController.js'  
import roleMiddleware from '../../middleware/roleMiddleware.js'
import authMiddleware from '../../middleware/authMiddleware.js'
const router = express.Router()

router.post("/create", roleMiddleware('ADMIN'), categoryController.createCategory);    
router.delete("/:id", roleMiddleware('ADMIN'), categoryController.deleteCategory);
router.post("/update/:id", roleMiddleware('ADMIN'), categoryController.updateCategory);
router.get("/getbyId/:id", authMiddleware,  categoryController.getById);
router.get("/getAll", authMiddleware, categoryController.getAll);

export default router;