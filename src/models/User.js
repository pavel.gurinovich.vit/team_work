import pkg from "mongoose"
const {model, Schema} = pkg

let userSchema = new Schema({
    email:{
        type: String,
        unique: true,
        required: true,
    },
    password:{
        type: String,
    },
    name:{
        type: String,
        required: true,
    },
    surname:{
        type: String,
        required: true,
    },
    role:{
        type: String,
        required: true,
        default: "USER"
    },
    order:[{
        type: Schema.Types.ObjectId,
        ref: 'Order'
    }],
    cart:{
        type: Schema.Types.ObjectId,
        ref: 'Cart',
    }
   
})


export default model('User', userSchema)