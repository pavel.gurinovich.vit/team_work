import pkg from "mongoose"
const {model, Schema} = pkg

const categorySchema = new Schema({
    name:{
        type: String,
        required: true,

    },
    products:[{
        type:Schema.Types.ObjectId,
        ref: 'Product'
    }]
})

export default model('Category', categorySchema)