import pkg from "mongoose"
const {model, Schema} = pkg


const cartSchema = new Schema({
    products: [{
        type: Schema.Types.ObjectId,
        ref: "Product",
    }],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
});
  
export default model('Cart', cartSchema)