import pkg from "mongoose"
const {model, Schema} = pkg

const productSchema = new Schema({
    name:{
        type: String,
    },
    price:{
        type: Number,
    },
    category:{
        type: Schema.Types.ObjectId,
        ref: 'Category'
    }
});

export default model('Product', productSchema)